package com.xiangyi.photoins.common;

public class MessageCenterException extends RuntimeException {

    public MessageCenterException(Msg msg, Exception exception){
        this.msg = msg;
        this.exception = exception;
    }

    private Exception exception;
    private Msg msg;

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public Msg getGeneralResponse() {
        return msg;
    }

    public void setGeneralResponse(Msg msg) {
        this.msg = msg;
    }
}
