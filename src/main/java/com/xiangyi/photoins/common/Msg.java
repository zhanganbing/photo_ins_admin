package com.xiangyi.photoins.common;


import com.xiangyi.photoins.common.enumeration.CodeEnum;

import java.io.Serializable;

/**
 * 通用封装类
 *
 * @author zab
 * @date 22:17
 */
public class Msg<T> implements Serializable {
    private static final long serialVersionUID = 7296635507708725688L;
    private String code;
    private T data;
    private String msg;

    public Msg() {
    }

    public Msg(String code, T data, String msg) {
        super();
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    public static Msg fail(String failMsg) {
        Msg msg = new Msg();
        msg.setMsg(failMsg);
        msg.setCode("1");
        msg.setData(null);
        return msg;
    }

    public static Msg success(String successMsg) {
        Msg msg = new Msg();
        msg.setMsg(successMsg);
        msg.setCode("0");
        msg.setData(null);
        return msg;
    }

    public static Msg success(Object data) {
        Msg msg = new Msg();
        msg.setMsg("success");
        msg.setCode("0");
        msg.setData(data);
        return msg;
    }

    public static Msg setResult(CodeEnum codeEnum) {
        Msg msg = new Msg();
        msg.setMsg(codeEnum.getMessage());
        msg.setCode(codeEnum.getCode());
        msg.setData(null);
        return msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}