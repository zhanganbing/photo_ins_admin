package com.xiangyi.photoins.common.enumeration;

/**
 * 返回码枚举
 *
 * @author zab
 * @date 20:27
 */
public enum CodeEnum {
    /**
     * 成功的请求
     */
    SUCCESS("0", "成功"),
    /**
     * 失败的请求
     */
    FAIL("1", "失败"),

    /**
     * 参数不匹配
     */
    PARAM_NOT_MATCH("2", "参数不匹配"),
    /**
     * 缺少必要参数
     */
    LACK_PARAM("3", "缺少必要参数"),

    /**
     * 数据查询失败
     */
    QUERY_FAIL("4", "数据查询失败"),

    /**
     * 数据插入失败
     */
    INSERT_FAIL("5", "数据插入失败"),

    /**
     * 数据删除失败
     */
    DELETE_FAIL("6", "数据删除失败"),

    /**
     * 数据更新失败
     */
    UPDATE_FAIL("7", "数据更新失败"),
    /**
     * 验证码不正确
     */
    CAPTCHA_NOT_CORRECT("8", "验证码不正确"),
    /**
     * 验证码不正确
     */
    RESULT_NOT_MATCH("9", "结果无匹配项"),
    /**
     * 不明运行时异常
     */
    INNER_ERROR("500", "客官稍等，程序员吊打中！");


    private final String code;
    private final String message;

    CodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }


    public String getMessage() {
        return message;
    }

}
