package com.xiangyi.photoins.service;

import com.xiangyi.photoins.common.Msg;
import com.xiangyi.photoins.param.PagingParam;
/**
* @author zab
* @date 2019-04-09 21:10
*/
public interface UserService {
    Msg findAll(PagingParam param);
}
