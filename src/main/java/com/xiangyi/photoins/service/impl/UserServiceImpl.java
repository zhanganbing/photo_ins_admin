package com.xiangyi.photoins.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.xiangyi.photoins.beans.PhotoInsUser;
import com.xiangyi.photoins.common.Msg;
import com.xiangyi.photoins.mapper.PhotoInsUserMapper;
import com.xiangyi.photoins.param.PagingParam;
import com.xiangyi.photoins.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author zab
* @date 2019-04-09 21:11
*/
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private PhotoInsUserMapper photoInsUserMapper;

    @Override
    public Msg findAll(PagingParam param) {
        PageHelper.startPage(param.getPageNum(),param.getPageSize());
        Page<PhotoInsUser> users = photoInsUserMapper.findAll();
        return new Msg("0",users,"查询成功");
    }
}
