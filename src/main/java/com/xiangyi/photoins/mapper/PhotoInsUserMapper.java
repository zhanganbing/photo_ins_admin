package com.xiangyi.photoins.mapper;

import com.github.pagehelper.Page;
import com.xiangyi.photoins.beans.PhotoInsUser;
import com.xiangyi.photoins.beans.PhotoInsUserExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface PhotoInsUserMapper {

    int countByExample(PhotoInsUserExample example);

    int deleteByExample(PhotoInsUserExample example);

    int insert(PhotoInsUser record);

    int insertSelective(PhotoInsUser record);

    List<PhotoInsUser> selectByExample(PhotoInsUserExample example);

    int updateByExampleSelective(@Param("record") PhotoInsUser record, @Param("example") PhotoInsUserExample example);

    int updateByExample(@Param("record") PhotoInsUser record, @Param("example") PhotoInsUserExample example);

    Page<PhotoInsUser> findAll();
}