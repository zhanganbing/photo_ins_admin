package com.xiangyi.photoins;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.xiangyi.photoins.mapper")
public class PhotoInsApplication {
	public static void main(String[] args) {
		SpringApplication.run(PhotoInsApplication.class, args);
	}
}
