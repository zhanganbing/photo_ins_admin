package com.xiangyi.photoins.web;

import com.xiangyi.photoins.common.Msg;
import com.xiangyi.photoins.param.PagingParam;
import com.xiangyi.photoins.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zab
 * @date 2019-04-09 21:05
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;


    /**
     * 分页查询所有用户
     *
     * @param [param]
     * @return com.xiangyi.photoins.common.Msg
     */
    @PostMapping("/find_all")
    public Msg findAll(PagingParam param) {
        if (param == null) {
            param = new PagingParam();
            param.setPageNum(1);
            param.setPageSize(10);
        }
        return userService.findAll(param);
    }
}
