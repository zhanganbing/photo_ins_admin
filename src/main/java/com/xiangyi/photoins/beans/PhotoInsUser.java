package com.xiangyi.photoins.beans;

import java.util.Date;

public class PhotoInsUser {
    private Integer userId;

    private String userName;

    private String userEmail;

    private String userAvatar;

    private Integer userGoldNum;

    private String isIns;

    private String userPwd;

    private String packageName;

    private String platform;

    private Integer insTime;

    private String insLanguage;

    private Date updateTime;

    private Date createTime;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail == null ? null : userEmail.trim();
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar == null ? null : userAvatar.trim();
    }

    public Integer getUserGoldNum() {
        return userGoldNum;
    }

    public void setUserGoldNum(Integer userGoldNum) {
        this.userGoldNum = userGoldNum;
    }

    public String getIsIns() {
        return isIns;
    }

    public void setIsIns(String isIns) {
        this.isIns = isIns == null ? null : isIns.trim();
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd == null ? null : userPwd.trim();
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName == null ? null : packageName.trim();
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform == null ? null : platform.trim();
    }

    public Integer getInsTime() {
        return insTime;
    }

    public void setInsTime(Integer insTime) {
        this.insTime = insTime;
    }

    public String getInsLanguage() {
        return insLanguage;
    }

    public void setInsLanguage(String insLanguage) {
        this.insLanguage = insLanguage == null ? null : insLanguage.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}